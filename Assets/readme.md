Description
-----------

This project tries to render an object in multiple render textures in a single frame, while 
changing the properties of the object for each render texture. We noticed a (probably) 
unintended behavior while doing so.

TLDR: read the conclusion

We also try to copy the render textures on Texture2D textures using AsyncGPUReadback.

The SampleScene has 9 sprite renderers. Their textures are rendered asynchronously.
We have a single text object, whose text  we change (we set it to some increasing number), 
render it to a render texture and assign to each sprite renderer. The numbers of the sprites 
top to bottom should be: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9.

Deploying the project in iOS and Android will show that the numbers appear in wrong order and will shuffle a bit.

In order to test the project in different renderers (more on that below) you must first
stop the project, and from the scene hierarchy, select the Renderer GameObject, go to the
Inspector and on the RTScheduler component, change the value of "render type".

You can read below what each render type does.

Some renderer types can work correctly on some platforms. You can find more on "results".

Implementation
---

Starting from "1" to "9" a separate render texture is used to have a TextMesh rendered on it. The RT is copied to a Texture2D and 
the result is assigned to the object's sprite renderer. All renders happen in 1 frame, apart from the RendererWithQueue
case. The whole process repeats every 20ms (interval can be adjusted in RTScheduler).

We noticed that the color of the resulting Texture2D is always correct. However, the number displayed sometimes is 
wrong. Specifically future numbers appears early on: e.g in the place of number "2", number "3" was displayed etc. 

We created 4 different renderers that try different ways to solve the problem.

All renderers programmatically create one (or multiple) GameObject(s) with a TextMesh component and one (or multiple) 
camera(s) to render on a RT and then copy it to Texture2D. 

For each render (request in code), the renderers update the TextMesh text and camera background color, 
call "camera.render()" to render to a RT (render texture) and finally call AsyncGPUReadback.Request() to get a Texture2D.

We also have a renderer that renders on the render texture without copying to a Texture2D. In this case, we just assign 
the final render texture to a mesh renderer.

Renderer Types
---

RendererNoSprite: Renders each request to a render texture.

RenderSimple: Renders each request to a RT and copies it to Texture2D. 

RendererWithQueue: Renders one request to a render texture per frame. Thus, 
multiple requests require multiple frames to be processed.

RendererWithMultipleGameObjects: Uses a different GameObject with a TextMesh component for each render.

RendererWithMultipleCameras: Uses a different camera for each render.

All renderers can wait until the end of frame before copying the render texture to the Texture2D. You can 
toggle this behavior for each renderer from the inspector's ""Copy after end of frame" switch.


You can read more at: RTScheduler.cs

Results
---

RendererNoSprite: Reproduces the issue in iOS and Android.

RenderSimple: Reproduces the issue in iOS and Android.

RendererWithQueue: Works correctly in iOS and Android.

RendererWithMultipleGameObjects: Works correctly in iOS and Android.

RendererWithMultipleCameras: Reproduces the issue in iOS and Android.

"Copy after end of frame" didn't seem to affect the results.

Conclusion
---

We believe that changing an object's properties, calling "camera.render()" and 
changing it again does not guarantee that the render will see the original 
state of the object.

