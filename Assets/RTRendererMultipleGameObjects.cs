﻿using UnityEngine;
using System;
using System.Collections;

public class RTRendererMultipleGameObjects : RTBaseRenderer
{
    private Camera _camera;
    private GameObject[] _textObjects = new GameObject[9];
    private TextMesh[] _textMeshes = new TextMesh[9];
    private int[] indexes = new int[9];

    [SerializeField]
    Font font;

    public bool copyAfterEndOfFrame = true;

    private static readonly bool DEBUG = false;

    private void OnEnable()
    {
        _camera = gameObject.AddComponent<Camera>();
        _camera.enabled = false;
        _camera.orthographic = true;
        _camera.renderingPath = RenderingPath.Forward;
        _camera.backgroundColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
        _camera.clearFlags = CameraClearFlags.SolidColor;
        _camera.useOcclusionCulling = false;
        _camera.allowMSAA = false;
        _camera.cullingMask = LayerMask.GetMask("OffRenderer");

        for (int i = 0; i < 9; i++)
        {

            _textObjects[i] = new GameObject("TextObject");
            _textObjects[i].name = "OffRenderer_Text_" + i;
            _textObjects[i].layer = LayerMask.NameToLayer("OffRenderer");
            _textObjects[i].AddComponent<MeshRenderer>();
            _textObjects[i].SetActive(true);
            _textObjects[i].transform.position = new Vector3(0, 0, 5);

            // Set the renderer material to the font material. Otherwise the font doesn't show up properly
            MeshRenderer topTextRenderer = _textObjects[i].GetComponent<MeshRenderer>();
            topTextRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            topTextRenderer.receiveShadows = false;
            topTextRenderer.lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off;
            topTextRenderer.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;
            topTextRenderer.sharedMaterial = font.material;

            _textMeshes[i] = _textObjects[i].AddComponent<TextMesh>();
            _textMeshes[i].characterSize = 0.1f;
            _textMeshes[i].anchor = TextAnchor.MiddleCenter;
            _textMeshes[i].alignment = TextAlignment.Center;
            _textMeshes[i].font = font;
            _textMeshes[i].lineSpacing = 0.6f;
            _textMeshes[i].color = Color.black;

            indexes[i] = i;
        }
    }

    public override void RequestRenderToRenderTexture(Color color, string text, Action<RenderTexture> callbackAction) { }

    public override void RequestRenderToTexture2D(Color color, string text, Action<Texture2D> callbackAction)
    {
        StartCoroutine(RenderRT(new Request()
        {
            color = color,
            text = text,
            callback = callbackAction
        }));
    }

    public static void Randomize<T>(T[] items)
    {
        // For each spot in the array, pick
        // a random item to swap into that spot.
        for (int i = 0; i < items.Length - 1; i++)
        {
            int j = UnityEngine.Random.Range(i, items.Length);
            T temp = items[i];
            items[i] = items[j];
            items[j] = temp;
        }
    }

    public void ShuffleGameObjects()
    {
        Randomize(indexes);
    }

    private IEnumerator RenderRT(Request request)
    {
        if (DEBUG) Debug.Log("RenderRT START -> " + request.text);

        int renderTextureWidth = 100;
        int renderTextureHeight = 20;

        _camera.backgroundColor = request.color;
        _camera.aspect = (float)renderTextureWidth / (float)renderTextureHeight;
        _camera.orthographicSize = renderTextureHeight / (2.0f * 100.0f);

        int index = indexes[int.Parse(request.text) - 1];

        for (int i = 0; i < 9; i++)
            _textObjects[i].SetActive(false);

        _textObjects[index].SetActive(true);
        _textMeshes[index].text = request.text;

        // Create the RenderTexture
        request.renderTexture = RenderTexture.GetTemporary(renderTextureWidth, renderTextureHeight, 0, UnityEngine.Experimental.Rendering.GraphicsFormat.R8G8B8A8_SRGB);
        request.renderTexture.filterMode = FilterMode.Point;
        //cameraRenderTexture.wrapMode = TextureWrapMode.Clamp;

        // Set the target texture of the camera to the camera render texture
        _camera.targetTexture = request.renderTexture;

        //var watch = Stopwatch.StartNew();

        // Render manually
        _camera.Render();
        //UnityEngine.Debug.Log(watch.ElapsedMilliseconds);

        if (copyAfterEndOfFrame)
        {
            yield return new WaitForEndOfFrame();
        }

        CopyToTexture2DAsync(request.renderTexture, (texture) =>
        {
            RenderTexture.ReleaseTemporary(request.renderTexture);
            request.renderTexture = null;

            request.callback(texture);

            if (DEBUG) Debug.Log("RenderRT DONE -> " + request.text);
        });

        yield return null;
    }
}
