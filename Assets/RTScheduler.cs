﻿using System.Collections;
using UnityEngine;


/// <summary>
/// All renderers use a TextMesh object and a camera to render on a RT and then copy it to Texture2D. 
/// For each request, they update the TextMesh text and camera background color, call "camera.render()" to render 
/// to a RT (render texture) and finally AsyncGPUReadback.Request() to get a Texture2D.
/// 
/// RenderSimple: Renders each request to a RT and copies it to Texture2D.
/// 
/// The rest of the renderes copy the RT to Texture2D after waiting for \"WaitForEndOfFrame()\".
/// 
/// RendererWithQueue: Renders one RT per frame. Thus, multiple requests require multiple frames to be processed.
/// 
/// RendererWithMultipleGameObjects: Uses a different TextMesh for each render.
/// 
/// RendererWithMultipleCameras: Uses a different camera for each render.
/// </summary>
enum RendererType
{
    RendererSimple,
    RendererWithQueue,
    RendererWithMultipleGameObjects,
    RendererWithMultipleCameras,
    RendererNoSprite
};

public class RTScheduler : MonoBehaviour
{
    [SerializeField]
    RendererType rendererType = RendererType.RendererWithQueue;
    RTBaseRenderer activeRenderer;

    public GameObject spriteParent;
    public GameObject quadParent;

    [Tooltip("Render Interval in seconds")]
    [Range(0.02f, 1.0f)]
    public float renderInterval = 0.2f;

    // Start is called before the first frame update
    void Start()
    {
        switch (rendererType)
        {
            case RendererType.RendererSimple:
                GetComponent<RTSimpleRenderer>().enabled = true;
                GetComponent<RTRendererWithQueue>().enabled = false;
                GetComponent<RTRendererMultipleGameObjects>().enabled = false;
                GetComponent<RTRendererMultipleCameras>().enabled = false;
                GetComponent<RTRendererNoSprite>().enabled = false;
                activeRenderer = GetComponent<RTSimpleRenderer>();
                break;

            case RendererType.RendererWithQueue:
                GetComponent<RTSimpleRenderer>().enabled = false;
                GetComponent<RTRendererWithQueue>().enabled = true;
                GetComponent<RTRendererMultipleGameObjects>().enabled = false;
                GetComponent<RTRendererMultipleCameras>().enabled = false;
                GetComponent<RTRendererNoSprite>().enabled = false;
                activeRenderer = GetComponent<RTRendererWithQueue>();
                break;

            case RendererType.RendererWithMultipleGameObjects:
                GetComponent<RTSimpleRenderer>().enabled = false;
                GetComponent<RTRendererWithQueue>().enabled = false;
                GetComponent<RTRendererMultipleGameObjects>().enabled = true;
                GetComponent<RTRendererMultipleCameras>().enabled = false;
                GetComponent<RTRendererNoSprite>().enabled = false;
                activeRenderer = GetComponent<RTRendererMultipleGameObjects>();
                break;
            case RendererType.RendererWithMultipleCameras:
                GetComponent<RTSimpleRenderer>().enabled = false;
                GetComponent<RTRendererWithQueue>().enabled = false;
                GetComponent<RTRendererMultipleGameObjects>().enabled = false;
                GetComponent<RTRendererMultipleCameras>().enabled = true;
                GetComponent<RTRendererNoSprite>().enabled = false;
                activeRenderer = GetComponent<RTRendererMultipleCameras>();
                break;
            case RendererType.RendererNoSprite:
                GetComponent<RTSimpleRenderer>().enabled = false;
                GetComponent<RTRendererWithQueue>().enabled = false;
                GetComponent<RTRendererMultipleGameObjects>().enabled = false;
                GetComponent<RTRendererMultipleCameras>().enabled = false;
                GetComponent<RTRendererNoSprite>().enabled = true;
                activeRenderer = GetComponent<RTRendererNoSprite>();
                break;
        }

        Debug.Log(rendererType);

        if (rendererType == RendererType.RendererNoSprite)
            StartCoroutine(RenderNoSprite());
        else
            StartCoroutine(Render());
    }

    IEnumerator RenderNoSprite()
    {
        quadParent.SetActive(true);
        spriteParent.SetActive(false);

        bool reuseMaterial = ((RTRendererNoSprite)activeRenderer).reuseMaterial;

        while (true)
        {
            // Renders each game object starting from "1" to "9" to a RT and
            // assigns it to the object's material's shader texture (this
            // happens synchronously).
            // Repeats the process every renderInterval ms
            foreach (Transform quadTransform in quadParent.transform)
            {
                GameObject quadGO = quadTransform.gameObject;
                RTData rtData = quadGO.GetComponent<RTData>();

                // Cleans the previously assigned rendertexture and material
                // so that the memory is deallocated
                rtData.CleanTemporaryData();

                activeRenderer.RequestRenderToRenderTexture(
                        rtData.color,
                        rtData.text,
                        (renderTexture) =>
                        {
                            // Set proper size
                            quadGO.transform.localScale = new Vector3(1, (float)renderTexture.height / (float)renderTexture.width, 1.0f);

                            if (reuseMaterial)
                            {
                                quadGO.GetComponent<Renderer>().material.SetTexture("_MainTex", renderTexture);

                                rtData.UpdateTemporaryData(renderTexture, null);
                            }
                            else
                            {
                                Shader shader = Shader.Find("Sprites/Default");
                                Material mat = new Material(shader);
                                mat.name = "Camera Texture";
                                mat.SetTexture("_MainTex", renderTexture);
                                quadGO.GetComponent<Renderer>().material = mat;

                                rtData.UpdateTemporaryData(renderTexture, mat);
                            }

                            
                        });
            }

            yield return new WaitForSeconds(renderInterval);
        }
    }

    IEnumerator Render()
    {
        quadParent.SetActive(false);
        spriteParent.SetActive(true);

        while (true)
        {
            // Renders each game object starting from "1" to "9" to a RT, copies it to Texture 2D and assigns to the object's sprite renderer (this happens asynchronously).
            // Repeats the process every renderInterval ms
            // The way rendering is implemented depends on the chosen Renderer type.
            foreach (Transform spriteTransform in spriteParent.transform)
            {
                GameObject spriteGO = spriteTransform.gameObject;
                RTData rtSpriteRenderer = spriteGO.GetComponent<RTData>();

                if (activeRenderer is RTRendererMultipleGameObjects)
                {
                    ((RTRendererMultipleGameObjects)activeRenderer).ShuffleGameObjects();
                }

                activeRenderer.RequestRenderToTexture2D(
                        rtSpriteRenderer.color,
                        rtSpriteRenderer.text,
                        (texture2D) =>
                        {
                            Sprite sprite = Sprite.Create(texture2D, new Rect(0.0f, 0.0f, texture2D.width, texture2D.height), new Vector2(0.5f, 0.5f));

                            if (spriteGO.GetComponent<SpriteRenderer>()?.sprite?.texture != null)
                            {
                                Destroy(spriteGO.GetComponent<SpriteRenderer>().sprite.texture);
                            }

                            spriteGO.GetComponent<SpriteRenderer>().sprite = sprite;
                        });
            }

            yield return new WaitForSeconds(renderInterval);
        }
    }
}
