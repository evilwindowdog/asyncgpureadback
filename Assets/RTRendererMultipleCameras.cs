﻿using UnityEngine;
using System;
using System.Collections;

public class RTRendererMultipleCameras : RTBaseRenderer
{
    private Camera[] _cameras = new Camera[9];
    private GameObject _textObject;
    private TextMesh _textMesh;

    [SerializeField]
    Font font;

    public bool copyAfterEndOfFrame = true;

    private void OnEnable()
    {
        for (int i = 0; i < 9; i++)
        {
            GameObject cameraObject = new GameObject("TextObject");
            cameraObject.name = "Camera_" + i;

            _cameras[i] = cameraObject.AddComponent<Camera>();
            _cameras[i].enabled = false;
            _cameras[i].orthographic = true;
            _cameras[i].renderingPath = RenderingPath.Forward;
            _cameras[i].backgroundColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
            _cameras[i].clearFlags = CameraClearFlags.SolidColor;
            _cameras[i].useOcclusionCulling = false;
            _cameras[i].allowMSAA = false;
            _cameras[i].cullingMask = LayerMask.GetMask("OffRenderer");
        }

        _textObject = new GameObject("TextObject");
        _textObject.name = "OffRenderer_Text";
        _textObject.layer = LayerMask.NameToLayer("OffRenderer");
        _textObject.AddComponent<MeshRenderer>();
        _textObject.SetActive(true);
        _textObject.transform.position = new Vector3(0, 0, 5);

        // Set the renderer material to the font material. Otherwise the font doesn't show up properly
        MeshRenderer topTextRenderer = _textObject.GetComponent<MeshRenderer>();
        topTextRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        topTextRenderer.receiveShadows = false;
        topTextRenderer.lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off;
        topTextRenderer.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;
        topTextRenderer.sharedMaterial = font.material;

        _textMesh = _textObject.AddComponent<TextMesh>();
        _textMesh.characterSize = 0.1f;
        _textMesh.anchor = TextAnchor.MiddleCenter;
        _textMesh.alignment = TextAlignment.Center;
        _textMesh.font = font;
        _textMesh.lineSpacing = 0.6f;
        _textMesh.color = Color.black;
    }

    public override void RequestRenderToRenderTexture(Color color, string text, Action<RenderTexture> callbackAction) { }

    public override void RequestRenderToTexture2D(Color color, string text, Action<Texture2D> callbackAction)
    {
        StartCoroutine(RenderRT(new Request()
        {
            color = color,
            text = text,
            callback = callbackAction
        }));
    }

    private IEnumerator RenderRT(Request request)
    {
        Debug.Log("RenderRT START -> " + request.text);

        int renderTextureWidth = 100;
        int renderTextureHeight = 20;

        int index = int.Parse(request.text) - 1;

        _cameras[index].backgroundColor = request.color;
        _cameras[index].aspect = (float)renderTextureWidth / (float)renderTextureHeight;
        _cameras[index].orthographicSize = renderTextureHeight / (2.0f * 100.0f);
        
        _textMesh.text = request.text;

        // Create the RenderTexture
        request.renderTexture = RenderTexture.GetTemporary(renderTextureWidth, renderTextureHeight, 0, UnityEngine.Experimental.Rendering.GraphicsFormat.R8G8B8A8_SRGB);
        request.renderTexture.filterMode = FilterMode.Point;
        //cameraRenderTexture.wrapMode = TextureWrapMode.Clamp;

        // Set the target texture of the camera to the camera render texture
        _cameras[index].targetTexture = request.renderTexture;

        //var watch = Stopwatch.StartNew();

        // Render manually
        _cameras[index].Render();
        //UnityEngine.Debug.Log(watch.ElapsedMilliseconds);

        if (copyAfterEndOfFrame)
        {
            yield return new WaitForEndOfFrame();
        }

        CopyToTexture2DAsync(request.renderTexture, (texture) =>
        {
            RenderTexture.ReleaseTemporary(request.renderTexture);
            request.renderTexture = null;

            request.callback(texture);

            Debug.Log("RenderRT DONE -> " + request.text);
        });

        yield return null;
    }
}
