﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class RTRendererWithQueue : RTBaseRenderer
{
    private Camera _camera;
    private GameObject _textObject;
    private TextMesh _textMesh;

    [SerializeField]
    Font font;

    public bool copyAfterEndOfFrame = true;

    private void OnEnable()
    {
        _camera = gameObject.AddComponent<Camera>();
        _camera.enabled = false;
        _camera.orthographic = true;
        _camera.renderingPath = RenderingPath.Forward;
        _camera.backgroundColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
        _camera.clearFlags = CameraClearFlags.SolidColor;
        _camera.useOcclusionCulling = false;
        _camera.allowMSAA = false;
        _camera.cullingMask = LayerMask.GetMask("OffRenderer");
        
        _textObject = new GameObject("TextObject");
        _textObject.name = "OffRenderer_Text";
        _textObject.layer = LayerMask.NameToLayer("OffRenderer");
        _textObject.AddComponent<MeshRenderer>();
        _textObject.SetActive(true);
        _textObject.transform.position = new Vector3(0, 0, 5);

        // Set the renderer material to the font material. Otherwise the font doesn't show up properly
        MeshRenderer topTextRenderer = _textObject.GetComponent<MeshRenderer>();
        topTextRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        topTextRenderer.receiveShadows = false;
        topTextRenderer.lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off;
        topTextRenderer.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;
        topTextRenderer.sharedMaterial = font.material;

        _textMesh = _textObject.AddComponent<TextMesh>();
        _textMesh.characterSize = 0.1f;
        _textMesh.anchor = TextAnchor.MiddleCenter;
        _textMesh.alignment = TextAlignment.Center;
        _textMesh.font = font;
        _textMesh.lineSpacing = 0.6f;
        _textMesh.color = Color.black;
    }

    private Queue<Request> requests = new Queue<Request>();
    private bool isRendering = false;

    private void Update()
    {
        if (isRendering)
            return;

        if (requests.Count == 0)
            return;

        isRendering = true;

        Request request = requests.Peek();

        StartCoroutine(RenderRT(request));
    }

    public override void RequestRenderToRenderTexture(Color color, string text, Action<RenderTexture> callbackAction) { }

    public override void RequestRenderToTexture2D(Color color, string text, Action<Texture2D> callbackAction)
    {
        Debug.Log("GenerateRT -> " + text);

        requests.Enqueue(new Request()
        {
            color = color,
            text = text,
            callback = callbackAction
        });
    }

    private IEnumerator RenderRT(Request request)
    {
        Debug.Log("RenderRT START -> " + request.text);

        if (!isRendering)
            yield return null;

        int renderTextureWidth = 100;
        int renderTextureHeight = 20;

        _camera.backgroundColor = request.color;
        _camera.aspect = (float)renderTextureWidth / (float)renderTextureHeight;
        _camera.orthographicSize = renderTextureHeight / (2.0f * 100.0f);

        _textMesh.text = request.text;

        // Create the RenderTexture
        request.renderTexture = RenderTexture.GetTemporary(renderTextureWidth, renderTextureHeight, 0, UnityEngine.Experimental.Rendering.GraphicsFormat.R8G8B8A8_SRGB);
        request.renderTexture.filterMode = FilterMode.Point;
        //cameraRenderTexture.wrapMode = TextureWrapMode.Clamp;

        // Set the target texture of the camera to the camera render texture
        _camera.targetTexture = request.renderTexture;

        //var watch = Stopwatch.StartNew();

        // Render manually
        _camera.Render();
        //UnityEngine.Debug.Log(watch.ElapsedMilliseconds);

        if (copyAfterEndOfFrame)
        {
            yield return new WaitForEndOfFrame();
        }

        CopyToTexture2DAsync(request.renderTexture, (texture) =>
        {
            RenderTexture.ReleaseTemporary(request.renderTexture);
            request.renderTexture = null;

            request.callback(texture);

            Debug.Log("RenderRT DONE -> " + request.text);

            requests.Dequeue();
            isRendering = false;
        });

        yield return null;
    }
}
