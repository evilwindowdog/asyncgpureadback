﻿using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class RTData : MonoBehaviour
{
    public Color color = Color.white;
    public string text = "test";

    #region Temporary data handling

    private RenderTexture renderTexture = null;
    private Material material = null;

    public void CleanTemporaryData()
    {
        if (renderTexture != null)
        {
            Destroy(renderTexture);
            renderTexture = null;
        }

        if (material != null)
        {
            Destroy(material);
            material = null;
        }
    }

    public void UpdateTemporaryData(RenderTexture renderTexture, Material material)
    {
        this.renderTexture = renderTexture;
        this.material = material;
    }

    private void OnDestroy()
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        if (spriteRenderer != null)
        {
            if (spriteRenderer.sprite?.texture != null)
            {
                Destroy(spriteRenderer.sprite.texture);
            }
        }

        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        if (meshRenderer != null)
        {
            if (meshRenderer?.material != null)
            {
                Destroy(meshRenderer.material.mainTexture);
                Destroy(meshRenderer.material);
            }
        }

    }

    #endregion
}
