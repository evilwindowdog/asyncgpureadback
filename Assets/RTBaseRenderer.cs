﻿using System;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Rendering;

public abstract class RTBaseRenderer : MonoBehaviour
{
    /// <summary>
    /// The implementation should render to a RenderTexture, call RequestTexture2D() and call the 
    /// provided callbackAction when done. 
    /// </summary>
    public abstract void RequestRenderToTexture2D(Color color, string text, Action<Texture2D> callbackAction);

    public abstract void RequestRenderToRenderTexture(Color color, string text, Action<RenderTexture> callbackAction);

    protected struct Request
    {
        public Color color;
        public string text;
        public Action<Texture2D> callback;
        public RenderTexture renderTexture;
    };

    /// <summary>
    /// Copies the provided render texture to a Texture2D asynchronously and calls callbackAction when done.
    /// </summary>
    public void CopyToTexture2DAsync(RenderTexture renderTexture, System.Action<Texture2D> callbackAction)
    {
        if (!SystemInfo.supportsAsyncGPUReadback)
        {
            Debug.Log("NOT SUPPORTED OMG");
            return;
        }

        AsyncGPUReadbackRequest rq = AsyncGPUReadback.Request(renderTexture,
            mipIndex: 0,
            callback: (request) =>
            {
                if (request.hasError)
                {
                    Debug.LogError("OMFG ERROR");
                    return;
                }

                if (!request.done)
                {
                    Debug.LogWarning("OMFG NOT DONE");
                    return;
                }

                NativeArray<Color32> data = request.GetData<Color32>();

                if (data.Length <= 0)
                {
                    Debug.LogWarning("NO DATA!");
                }
                {
                    var tex = new Texture2D(request.width, request.height, TextureFormat.RGBA32, false);
                    tex.SetPixels32(data.ToArray());
                    tex.filterMode = FilterMode.Point;
                    tex.wrapMode = TextureWrapMode.Clamp;
                    tex.Apply();

                    callbackAction(tex);
                }
            });
    }
}
